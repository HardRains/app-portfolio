package com.hardrains.appportfolio;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity
{
    Map<View, String> mButtons = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void showToast(View view)
    {
        String tag = (String)view.getTag();
        if(tag.substring(0,3).equals("com"))
        {
            Intent launchIntent = getPackageManager().getLaunchIntentForPackage(tag);
            if(launchIntent != null)
            {
                startActivity(launchIntent);
            }
            return;
        }

        Toast.makeText(getApplicationContext(), (String)view.getTag(), Toast.LENGTH_LONG).show();
    }
}
